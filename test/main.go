package main

import "../../flag"

func main() {
	// Required flag
	fname := flag.StrR("fname")
	// Optional flag with default
	xmin := flag.F64D("xmin", 0.5)
	xmax := flag.F64D("xmax", 13.37)
	a := flag.F64D("a", 2)
	b := flag.F64D("b", -1)
	// Check for unused flags
	flag.Unused()
	// Do something with the flag-values
	x := make([]float64, 10)
	y := make([]float64, 10)
	for i := range x {
		x[i] = float64(i)*(xmax-xmin)/10 + xmin
		y[i] = a*x[i] + b
	}
	// Save a self describing data-file
	flag.SaveData(fname, ",y", x, y)
}

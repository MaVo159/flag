package flag

import "os"
import "fmt"
import "strconv"
import "encoding/json"
import "reflect"
import "strings"
import "io"

type FlagMap map[string]struct {
	value          string
	provided, used bool
}

func (m FlagMap) MarshalJSON() ([]byte, error) {
	if len(m) > 0 {
		str := "{"
		for flag, val := range m {
			str += fmt.Sprintf("\"%s\":\"%s\",", flag, val.value)
		}
		str = str[0:len(str)-1] + "}"
		return []byte(str), nil
	}
	return []byte("{}"), nil
}

var Flags FlagMap

func init() {
	Flags = make(map[string]struct {
		value          string
		provided, used bool
	}, len(os.Args)/2)
	for i := 1; i < len(os.Args); i += 2 {
		if os.Args[i][0] != '-' {
			fmt.Fprint(os.Stderr, "Expected flag, found \"", os.Args[i], "\".\n")
			os.Exit(1)
		}
		if _, ok := Flags[os.Args[i][1:]]; ok {
			fmt.Fprint(os.Stderr, "Dublicate flag \"", os.Args[i], "\".\n")
			os.Exit(1)
		}
		if len(os.Args) < i+2 {
			fmt.Fprint(os.Stderr, "Missing value for flag \"", os.Args[i], "\".\n")
			os.Exit(1)
		}
		Flags[os.Args[i][1:]] = struct {
			value          string
			provided, used bool
		}{os.Args[i+1], true, false}
	}
}

// Exits with error if flag not provided.
func R(flag string) {
	if !Flags[flag].provided {
		fmt.Fprintln(os.Stderr, "Missing flag:", flag)
		os.Exit(1)
	}
}

// Parses flag value as any of the supported types if present and stores result in provided pointer.
// If the flag is not present, the value the pointer points to is stored as flag (default).
func parseAny(flag string, p interface{}) {
	// Get value of pointer argument a reflect.Value
	rp := reflect.ValueOf(p)
	// Make sure pointer argument is actually a pointer
	if rp.Kind() != reflect.Ptr {
		panic("not a pointer")
	}
	// Retrieve information about flag
	flagInfo := Flags[flag]
	//Parse flag value string or store default as string in flag map
	if !flagInfo.provided {
		flagInfo.value = fmt.Sprint(rp.Elem())
	} else {
		var err error
		switch tp := p.(type) {
		case *float32:
			var tmp float64
			tmp, err = strconv.ParseFloat(flagInfo.value, 32)
			*tp = float32(tmp)
		case *float64:
			*tp, err = strconv.ParseFloat(flagInfo.value, 64)
		case *string:
			*tp = flagInfo.value
		case *int:
			*tp, err = strconv.Atoi(flagInfo.value)
		default:
			panic("unexpected type")
		}
		if err != nil {
			fmt.Fprintln(os.Stderr, "Invalid", rp.Type().Elem(), "flag value:", flagInfo.value)
			os.Exit(1)
		}
	}
	// Mark flag as used and write back changes to flag map
	flagInfo.used = true
	Flags[flag] = flagInfo
}

// Returns flag value as float32. Exits with error if flag not provided.
func F32R(flag string) float32 {
	R(flag)
	return F32D(flag, 0)
}

// Returns flag value as float64. If flag not present default value is returned.
func F32D(flag string, def float32) float32 {
	parseAny(flag, &def)
	return def
}

// Returns flag value as float64. Exits with error if flag not provided.
func F64R(flag string) float64 {
	R(flag)
	return F64D(flag, 0)
}

// Returns flag value as float64. If flag not present default value is returned.
func F64D(flag string, def float64) float64 {
	parseAny(flag, &def)
	return def
}

// Returns flag value as string. Exits with error if flag not provided.
func StrR(flag string) string {
	R(flag)
	return StrD(flag, "")
}

// Returns flag value as string. If flag not present default value is returned.
func StrD(flag string, def string) string {
	parseAny(flag, &def)
	return def
}

// Returns flag value as string. Exits with error if flag not provided.
func IntR(flag string) int {
	R(flag)
	return IntD(flag, 0)
}

// Returns flag value as string. If flag not present default value is returned.
func IntD(flag string, def int) int {
	parseAny(flag, &def)
	return def
}

// Exits with error if not all provided flags were read.
func Unused() {
	var unused bool
	for flag, val := range Flags {
		if val.provided && !val.used {
			fmt.Fprint(os.Stderr, "Flag \"", flag, "\" provided but not used.")
			unused = true
		}
	}
	if unused {
		os.Exit(1)
	}
}

// Write values of all arguments to file (INI format).
func Save(fn string) {
	f, err := os.Create(fn)
	for flag, val := range Flags {
		if err != nil {
			break
		}
		_, err = fmt.Fprintln(f, flag, "=", val.value)
	}
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error saving flags to file:", err)
		os.Exit(1)
	}
}

// Write flags and provided data to file with name fn.
func SaveData(fn, names string, data ...interface{}) {
	// Create file and encoder
	f, err := os.Create(fn)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating data-file:", err)
		os.Exit(1)
	}
	enc := json.NewEncoder(f)

	// Define function that check and prints writing errors
	checkErr := func(err error) {
		if err != nil {
			fmt.Fprintln(os.Stderr, "Error writing to file:", err)
			os.Exit(1)
		}
	}

	// Define function that adds json field
	addField := func(name string, data interface{}) {
		_, err := fmt.Fprint(f, "\"", name, "\":")
		checkErr(err)
		err = enc.Encode(data)
		checkErr(err)
	}

	// Start json object
	_, err = fmt.Fprintln(f, "{")
	checkErr(err)

	// Write flags to file
	addField("Metadata", Flags)
	fmt.Fprint(f, ",")

	// Write named data to file
	unnamed := make([]interface{}, 0, len(data))
	splitNames := strings.Split(names, ",")
	if len(splitNames) > len(data) {
		splitNames = splitNames[0:len(data)]
	}
	for i, name := range splitNames {
		name := strings.TrimSpace(name)
		if name != "" {
			addField(name, data[i])
			fmt.Fprint(f, ",")
		} else {
			unnamed = append(unnamed, data[i])
		}
	}

	// Write unnamed data to file
	unnamed = append(unnamed, data[len(splitNames):]...)
	// 	if len(unnamed) > 1 {
	// 		addField("Data", unnamed)
	// 	} else if len(unnamed) == 1 {
	// 		addField("Data", unnamed[0])
	// 	}
	addField("Data", unnamed)

	// End json object and close file
	_, err = fmt.Fprintln(f, "}")
	checkErr(err)
	err = f.Close()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error closing data-file:", err)
		os.Exit(1)
	}
}

// JSON-encoding of single-values, lists, and maps. No linebreaks.
// Pass reference types (pointers or slices) to avoid copies.
func Encode2json(w io.Writer, x interface{}) {
	value2json(w, reflect.ValueOf(x))
}

func value2json(w io.Writer, v reflect.Value) {
	// Follow pointers and unpack interfaces
	for v.Kind() == reflect.Ptr || v.Kind() == reflect.Interface {
		v = v.Elem()
	}
	// Encode other Kinds
	switch v.Kind() {
	// Single element types:
	case reflect.Bool:
		fmt.Fprint(w, v)
	case reflect.String:
		//TODO: Deal with control characters like \n by escaping. See json.org
		fmt.Fprintf(w, "%q", v)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		fmt.Fprint(w, v)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		fmt.Fprint(w, v)
	case reflect.Float32, reflect.Float64:
		fmt.Fprint(w, v)
	// Lists:
	case reflect.Slice, reflect.Array:
		fmt.Fprint(w, "[")
		if v.Len() > 0 {
			value2json(w, v.Index(0))
			for i := 1; i < v.Len(); i++ {
				fmt.Fprint(w, ",")
				value2json(w, v.Index(i))
			}
		}
		fmt.Fprint(w, "]")
	case reflect.Complex64, reflect.Complex128:
		panic("Complex") //TODO: Encode complex numbers as 2 element lists
	// Objects:
	case reflect.Map:
		keys := v.MapKeys()
		fmt.Fprint(w, "{")
		if len(keys) > 0 {
			var key_name string = fmt.Sprint(keys[0]) // I do this because JSON only accepts double-quoted string names.
			fmt.Fprintf(w, "%q:", key_name)
			value2json(w, v.MapIndex(keys[0]))
			for i := 1; i < len(keys); i++ {
				key_name = fmt.Sprint(keys[i])
				fmt.Fprintf(w, ",%q:", key_name)
				value2json(w, v.MapIndex(keys[i]))
			}
		}
		fmt.Fprint(w, "}")
	case reflect.Struct:
		fmt.Fprint(w, "{")
		for i := 0; i < v.NumField(); i++ {
			if i > 0 {
				fmt.Fprint(w, ",")
			}
			fmt.Fprintf(w, "%q:", v.Type().Field(i).Name)
			value2json(w, v.Field(i))
		}
		fmt.Fprint(w, "}")
		// null
	case reflect.UnsafePointer, reflect.Func, reflect.Chan:
		fmt.Fprint(w, "null")
	default:
		panic(fmt.Sprintln("Unknown Kind:", v.Kind()))
	}
}

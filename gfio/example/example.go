// The idea with this package is to combine the input featurs of "flag" with the construction of a data-structure which can easily be converted to json and saved
package main

import (
	"fmt"
	"../"
)

func main() {
 	f := gfio.F64("f", 13.37)
 	i := gfio.Int("i", 42, "Her er en beskrivelse")
 	s := gfio.Str("s", "default", "her er også noget beskrivelse", "og her er mere")
 	lsn := gfio.Int(
 		"flag_with_a_super_long_silly_name",
 		1,
 		"some description which also takes up space",
 	)
	gfio.EnableHelp()

	dl1 := gfio.NewDataList("datalist")
	dl1.Append(17)
	dl1.Append("string")
	dl1.Append(map[string]float64{"lead":13.37, "another number":1.2})

	dd1 := gfio.NewDataDict("datadict")
	dd1.Append("name","Data dictionary")
	dd1.Append("age","Pretty young")
	dd1.Append("some list",[]float64{1,2,3,13.37,42})
	
	gfio.SaveDataJSON("data.json","explicit_variable,bla,blabla",12)
	
	fmt.Println(f,i,s,lsn)
}

// The idea with this package is to combine the input featurs of "flag" with the construction of a data-structure which can easily be converted to json and saved
package gfio

import (
	"fmt"
	"os"
	"strings"
)

type flagT struct{
	val, typ, def   string
	provided, used  bool
	description     []string
}

var flags map[string]flagT
var flagnames []string // only used flags
var tmpDataFiles []tmpdatafile

func init() {
	// init Flag-map
	flags = make(map[string]flagT)
	flagnames = make([]string,0)
	// init list of temporary data files
	tmpDataFiles = make([]tmpdatafile,0)
	// read arguement-list and add arguements to provided flags
	for i := 1; i < len(os.Args); i++ {
		arg := strings.SplitN(os.Args[i],"=",2)
		if flags[arg[0]].provided  {
			fmt.Fprintf(os.Stderr,
				"Flag provided more than once: %q\n", arg[0],
			)
			os.Exit(1)
		}
		flagInfo := flagT{}
		flagInfo.provided = true
		if len(arg) > 1 {
			flagInfo.val = arg[1]
		}
		flags[arg[0]] = flagInfo
	}
}

package gfio

import (
	"io"
	"reflect"
	"fmt"
)

// JSON-encoding of single-values, lists, and maps. No linebreaks.
// Pass reference types (pointers or slices) to avoid copies.
func Encode2json(w io.Writer, x interface{}) {
	value2json(w, reflect.ValueOf(x))
}

func value2json(w io.Writer, v reflect.Value) {
	// Follow pointers and unpack interfaces
	for v.Kind() == reflect.Ptr || v.Kind() == reflect.Interface {
		v = v.Elem()
	}
	// Encode other Kinds
	switch v.Kind() {
	// Single element types:
	case reflect.Bool:
		fmt.Fprint(w, v)
	case reflect.String:
		//TODO: Deal with control characters like \n by escaping. See json.org
		fmt.Fprintf(w, "%q", v)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		fmt.Fprint(w, v)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		fmt.Fprint(w, v)
	case reflect.Float32, reflect.Float64:
		fmt.Fprint(w, v)
	// Lists:
	case reflect.Slice, reflect.Array:
		fmt.Fprint(w, "[")
		if v.Len() > 0 {
			value2json(w, v.Index(0))
			for i := 1; i < v.Len(); i++ {
				fmt.Fprint(w, ",")
				value2json(w, v.Index(i))
			}
		}
		fmt.Fprint(w, "]")
//	case reflect.Complex64, reflect.Complex128:
//		fmt.Fprintf(w,"[%s,%s]",real(v),imag(v))
	// Objects:
	case reflect.Map:
		keys := v.MapKeys()
		fmt.Fprint(w, "{")
		if len(keys) > 0 {
			var key_name string = fmt.Sprint(keys[0]) // I do this because JSON only accepts double-quoted string names.
			fmt.Fprintf(w, "%q:", key_name)
			value2json(w, v.MapIndex(keys[0]))
			for i := 1; i < len(keys); i++ {
				key_name = fmt.Sprint(keys[i])
				fmt.Fprintf(w, ",%q:", key_name)
				value2json(w, v.MapIndex(keys[i]))
			}
		}
		fmt.Fprint(w, "}")
	case reflect.Struct:
		fmt.Fprint(w, "{")
		for i := 0; i < v.NumField(); i++ {
			if i > 0 {
				fmt.Fprint(w, ",")
			}
			fmt.Fprintf(w, "%q:", v.Type().Field(i).Name)
			value2json(w, v.Field(i))
		}
		fmt.Fprint(w, "}")
		// null
	case reflect.UnsafePointer, reflect.Func, reflect.Chan:
		fmt.Fprint(w, "null")
	default:
		panic(fmt.Sprintln("Unknown Kind:", v.Kind()))
	}
}
